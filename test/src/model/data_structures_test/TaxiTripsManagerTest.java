package model.data_structures_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;

public class TaxiTripsManagerTest {

	public static final String DIR_TEST = "./data/Test.json";

	private DoubleLinkedList<Servicio> dll;

	private TaxiTripsManager ttm;

	private Servicio[] svs = new Servicio[0];
	//			{
	//			new Servicio("srvc_1", "tx_1", "cash", "theCompany"),
	//			new Servicio("srvc_2", "tx_1", "cash", "theCompany"),
	//			new Servicio("srvc_3", "tx_2", "cash", "theCompany"),
	//			new Servicio("srvc_4", "tx_3", "cash", "otherCompany"),
	//			new Servicio("srvc_5", "tx_2", "cash", "otherCompany"),
	//			new Servicio("srvc_6", "tx_4", "cash", "theCompany")};

	@Before
	public void setupEscenario1(){
		ttm = new TaxiTripsManager();
		ttm.cargarSistema(DIR_TEST);
	}

	@Test
	public void testAdd(){
		setupEscenario1();
		assertEquals("No carg� todos los elementos", 3, dll.size());
	}

	@Test
	public void get(){
		assertEquals("El get est� mal", svs[0], dll.get(1));
		assertEquals("El get est� mal", svs[0], dll.get(svs[0]));
		assertNull("no deber�a habr nada en esa posicion", dll.get(7));
		assertNull("no deber�a habr nada en esa posicion", dll.get(svs[5]));
	}

	@Test
	public void testDel(){
		//		dll.delete(element)
		assertEquals("No carg� todos los elementos", 3, dll.size());
	}
}
