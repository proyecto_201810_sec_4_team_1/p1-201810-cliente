package model.data_structures_test;

import model.data_structures.DoubleLinkedList;
import model.vo.Servicio;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DoubleLinkedListTest {

	private DoubleLinkedList<Servicio> dll;

	@Before
	public void setupEscenario1(){
		dll  = new DoubleLinkedList<>();
	}


	public void setupEscenario2(){
		setupEscenario1();
		Servicio s1 = new Servicio();
		Servicio s2 = new Servicio();
		Servicio s3 = new Servicio();
		s1.setId("servicio1");
		s2.setId("servicio2");
		s3.setId("servicio3");
		dll.add(s1);
		dll.add(s2);
		dll.add(s3);
	}
	public void setupEscenario3(){
		setupEscenario2();
		Servicio s4 = new Servicio();
		Servicio s5 = new Servicio();
		s4.setId("servicio4");
		s5.setId("servicio5");
		dll.add(s4);
		dll.add(s5);
	}

	@Test
	public void testAdd(){
		setupEscenario2();
		assertEquals("No carg� todos los elementos", 3, dll.size());
		setupEscenario3();
		assertEquals("No carg� todos los elementos", 5, dll.size());
	}

	@Test
	public void get(){
		setupEscenario2();
		assertEquals("El get(#) est� mal", "servicio1", dll.get(1).getTripId());
		Servicio s = new Servicio(); s.setId("servicio3");
		Servicio s2 = new Servicio(); s2.setId("servicio8");
		assertEquals("El get(T) est� mal", s, dll.get(s));
		assertNull("no deber�a habr nada en esa posicion", dll.get(7));
		assertNull("no deber�a habr nada en esa posicion", dll.get(s2));
	}

	@Test
	public void testDel(){
		setupEscenario2();
		//		dll.delete(element)
		assertEquals("No carg� todos los elementos", 3, dll.size());
	}

}
