package model.vo;

import model.data_structures.LinkedList;

public class InfoCiudadRango {

	private String idCity;

	private RangoFechaHora rango;

	private double plataGanadaDiferenteZona;

	private double plataGanadaMismaZona;

	private double plataGanadaDiferenteZonaI;

	private LinkedList<Servicio> serviciosPrestadosEnRangoDiferente;

	private LinkedList<Servicio> serviciosPrestadosEnRangoMisma;

	private LinkedList<Servicio> serviciosPrestadosEnRangoDiferenteI;

	public String getIdCity(){
		return idCity;
	}

	public void setIdCity(String idCity)
	{
		this.idCity = idCity;
	}

	public RangoFechaHora getRango(){
		return rango;
	}

	public void setRango(RangoFechaHora rango){
		this.rango = rango;
	}

	public double getPlataGanadaDiferenteZona(){
		return plataGanadaDiferenteZona;
	}

	public void setPlataGanadaDiferenteZona(double plataGanada){
		this.plataGanadaDiferenteZona = plataGanada;
	}

	public double getPlataGanadaDiferenteZonaI(){
		return plataGanadaDiferenteZonaI;
	}

	public void setPlataGanadaDiferenteZonaI(double plataGanada){
		this.plataGanadaDiferenteZonaI = plataGanada;
	}

	public double getPlataGanadaMismaZona(){
		return plataGanadaMismaZona;
	}

	public void setPlataGanadaMismaZona(double plataGanada){
		this.plataGanadaMismaZona = plataGanada;
	}

	public LinkedList<Servicio> getServiciosPrestadosEnRangoDiferente(){
		return serviciosPrestadosEnRangoDiferente;
	}

	public void setServiciosPrestadosEnRangoDiferente(LinkedList<Servicio> serviciosPrestadosEnRango){
		this.serviciosPrestadosEnRangoDiferente = serviciosPrestadosEnRango;
	}
	public int numeroTotalServiciosDiferente(){
		return	serviciosPrestadosEnRangoDiferente.size();
	}

	public LinkedList<Servicio> getServiciosPrestadosEnRangoMisma(){
		return serviciosPrestadosEnRangoMisma;
	}

	public void setServiciosPrestadosEnRangoMisma(LinkedList<Servicio> serviciosPrestadosEnRango){
		this.serviciosPrestadosEnRangoMisma = serviciosPrestadosEnRango;
	}
	public int numeroTotalServiciosMisma(){
		return	serviciosPrestadosEnRangoMisma.size();
	}

	public LinkedList<Servicio> getServiciosPrestadosEnRangoDiferenteI(){
		return serviciosPrestadosEnRangoDiferenteI;
	}

	public void setServiciosPrestadosEnRangoDiferenteI(LinkedList<Servicio> serviciosPrestadosEnRango){
		this.serviciosPrestadosEnRangoDiferenteI = serviciosPrestadosEnRango;
	}
	public int numeroTotalServiciosDiferenteI(){
		return	serviciosPrestadosEnRangoDiferenteI.size();
	}
}
