package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	
	private String trip_id;

	private String taxi_id;

	private String company;

	//	______________________

	private String trip_start_timestamp;

	private String trip_end_timestamp;

	private int dropoff_community_area;

	private int pickup_community_area;

	private double trip_miles;

	private double trip_total;

	private int trip_seconds;

	public void setId(String id){
		this.trip_id = id;
	}

	public String getPickArea(){
		return Integer.toString(pickup_community_area);
	}
	public String  getDropArea(){
		return Integer.toString(dropoff_community_area);
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId(){
		// TODO Auto-generated method stub
		return trip_id;}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;}

	public String getTripStart() {
		// TODO Auto-generated method stub
		return trip_start_timestamp;}

	public String getTripEnd() {
		// TODO Auto-generated method stub
		return trip_end_timestamp;}

	public String getCompany(){
		return company;
	}

	public boolean estaEnRango(RangoFechaHora rango){
		boolean esta1 = false;
		boolean esta2 = false;
		String fechaIn = rango.getFechaInicial() +"T"+ rango.getHoraInicio();
		String fechaFn = rango.getFechaFinal() + "T" + rango.getHoraFinal();
		if(trip_start_timestamp != null && trip_start_timestamp.compareTo(fechaIn) >= 0)
			esta1 = true;
		if(trip_end_timestamp != null && (trip_end_timestamp.compareTo(fechaFn)<=0))esta2 = true;
		return (esta1 && esta2);}

	public boolean estaEnRango2(RangoFechaHora rango){
		boolean esta = false;
		String fechaIn = rango.getFechaInicial() +"T"+ rango.getHoraInicio();
		String fechaFn = rango.getFechaFinal() + "T" + rango.getHoraFinal();
		if(trip_start_timestamp != null){
			if((trip_start_timestamp.compareTo(fechaIn) >= 0)&&
					(trip_start_timestamp.compareTo(fechaFn)<=0))
				esta = true;
		}
		return esta;}

	@Override
	public int compareTo(Servicio arg0) {
		return (this.trip_id.compareTo(arg0.getTripId())<0)?-1:
			(this.trip_id.compareTo(arg0.getTripId())>0)?1:0;}

	public int compareToDate(Servicio arg0) {
		return (this.trip_start_timestamp.compareTo(arg0.getTripStart())<0)?-1:
			(this.trip_start_timestamp.compareTo(arg0.getTripStart())>0)?1:0;}
}
