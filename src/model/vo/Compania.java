package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;

public class Compania implements Comparable<Compania> {

	private String nombre;

	private LinkedList<Taxi> taxisInscritos;	

	public Compania(){
		nombre = "";
		taxisInscritos = new DoubleLinkedList<>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(LinkedList<Taxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}

	public boolean existeTaxi(String  t)
	{
		while(((DoubleLinkedList<Taxi>)taxisInscritos).hasNext())
		{
			if(taxisInscritos.next().getTaxiId().equals(t))
			{
				return true;
			}
			taxisInscritos.next();
		}
		return false;
	}

	@Override
	public int compareTo(Compania o) {
		return (this.nombre != null)?this.nombre.compareTo(o.getNombre()):0;
	}




}
