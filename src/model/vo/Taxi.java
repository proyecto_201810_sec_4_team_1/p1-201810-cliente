package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.LinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{
	private String taxi_id;

	private String company;

	private DoubleLinkedList<Servicio> serviciosEnRango;

	public Taxi(String id, String company){
		taxi_id = id;
		this.company = company;
		serviciosEnRango = new DoubleLinkedList<>();
	}

	public LinkedList<Servicio> servicios(){
		return serviciosEnRango;
	}

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId(){
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany(){
		// TODO Auto-generated method stub
		return company;
	}

	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}