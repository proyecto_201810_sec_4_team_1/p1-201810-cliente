package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * @authors <b>Anderson Barrag�n 201719821</b> && <b>mario hurtado</b>
 */
public class TaxiTripsManager implements ITaxiTripsManager {

	//	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "large";
	public static final String DIRECCION_LARGE= "./data/Large/taxi-trips-wrvz-psew-subset-0";
	public static final String COMPLEMENT_DIR = "-02-2017.json";
	public static final String DIRECCION_TEST_JSON = "./data/Test.json";
	public static final int CANTIDAD_ARCHIVOS = 7;

	private DoubleLinkedList<Servicio> ListaServicios = new DoubleLinkedList<>();

	@Override //1C
	public boolean cargarSistema(String direccionJson) {
		boolean load = false;
		Gson gson = new GsonBuilder().create();
		try{
			if(direccionJson.equals(DIRECCION_LARGE_JSON))
				cargaSML(gson, direccionJson, 2);
			else cargaSML(gson, direccionJson, 1);
			load = true;
		}catch(Exception e){System.out.println("Error al cargar el archivo");}
		return load;}


	@Override //1A ANDERSON BARRAGAN
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango){
		Queue<Servicio> Srvcs = new Queue<>();
		DoubleLinkedList<Servicio> temp = new DoubleLinkedList<>();
		while(ListaServicios.hasNext()){
			Servicio a = ListaServicios.getCurrent();
			if(a.estaEnRango(rango)){
				//System.out.println(a.getTaxiId());
				temp.add(a);
			}
			ListaServicios.next();
		}if(!(temp.isEmpty())){
			temp.sortByDate();
			while(temp.hasNext()){
				Servicio a = temp.getCurrent();
				Srvcs.enqueue(a);
				temp.next();
			}
		}temp = null;
		return Srvcs;
	}

	@Override //2A ANDERSON BARRAGAN
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company){
		Taxi elMasChingon = null;
		DoubleLinkedList<Taxi> t = new DoubleLinkedList<>();
		while(ListaServicios.hasNext()){
			Servicio a = ListaServicios.getCurrent();
			if(a.getCompany() != null && a.getCompany().contains(company) && a.estaEnRango2(rango)){
				Taxi tax = new Taxi(a.getTaxiId(), a.getCompany());
				Taxi elT = t.get(tax);
				if(elT != null){
					elT.servicios().add(a);
				}else{
					tax.servicios().add(a);
					t.add(tax);}}
			ListaServicios.next();
		}
		if(!t.isEmpty()){
			elMasChingon = t.get(1);
			while(t.hasNext()){
				if(t.getCurrent().servicios().size() > elMasChingon.servicios().size()){
					elMasChingon = t.getCurrent();}
				t.next();
			}
		}
		return elMasChingon;
	}

	@Override //3A ANDERSON BARRAGAN
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango){
		InfoTaxiRango it = new InfoTaxiRango();
		it.setServiciosPrestadosEnRango(new DoubleLinkedList<Servicio>());
		it.setIdTaxi(id);
		it.setRango(rango);
		while(ListaServicios.hasNext()){
			Servicio a = ListaServicios.getCurrent();
			//			String[] fhIn = a.getTripStart().split("T");
			if(a.getTaxiId().equals(it.getIdTaxi())&& a.estaEnRango(rango)){
				it.getServiciosPrestadosEnRango().add(a);
				it.setRango(rango);
				it.setDistanciaTotalRecorrida(it.getDistanciaTotalRecorrida() + a .getTripMiles());
				it.setPlataGanada(it.getPlataGanada() + a.getTripTotal());
				it.setTiempoTotal(it.getTiempoTotal() + a.getTripSeconds());
			}
			ListaServicios.next();
		}
		return it;
	}

	@Override //4A ANDERSON BARRAGAN
	public LinkedList<RangoDistancia> darListaRangosDistancia
	(String fecha, String horaInicial, String horaFinal) {
		DoubleLinkedList<RangoDistancia> r = new DoubleLinkedList<RangoDistancia>();
		while(ListaServicios.hasNext()){
			Servicio s = ListaServicios.getCurrent();
			String[] fhs = s.getTripStart().split("T");
			if(s.getTripEnd() != null){
				String[] fhe = s.getTripEnd().split("T");

				if((fhs[0].compareTo(fecha) >= 0)&&(fhs[1].compareTo(horaInicial)>=0 && fhe[1].compareTo(horaFinal)<=0)){
					RangoDistancia rd = new RangoDistancia();
					rd.setLimineInferior(Math.floor(s.getTripMiles()));
					rd.setLimiteSuperior(Math.ceil(s.getTripMiles()));
					rd.setServiciosEnRango(new DoubleLinkedList<Servicio>());
					RangoDistancia rd2 = r.get(rd);
					if(rd2 != null){
						rd2.getServiciosEnRango().add(s);
					}else{
						rd.getServiciosEnRango().add(s);
						r.add(rd);
					}
				}}
			ListaServicios.next();
		}
		return r;
	}

	@Override //1B MARIO HURTADO 
	public LinkedList<Compania> darCompaniasTaxisInscritos(){
		DoubleLinkedList< Compania> a = new DoubleLinkedList<Compania>();
		while (ListaServicios.hasNext()){
			Servicio s = ListaServicios.getCurrent();
			if(s.getCompany()!=null){
				if (a.isEmpty()){
					Compania com = new Compania();
					com.setNombre(s.getCompany());
					com.getTaxisInscritos().add(new Taxi(s.getTaxiId(),s.getCompany()));
					a.add(com);        	    	
				}
				else if (!a.isEmpty()){
					if (existeCompania(s.getCompany(), a)){ 	   
						while (a.hasNext()){
							Compania comp = a.getCurrent();
							if ((comp != null) &&(s.getCompany() != null) && comp.equals(s.getCompany())){
								if (!comp.existeTaxi(s.getTaxiId())){
									comp.getTaxisInscritos().add(new Taxi(s.getTaxiId(), s.getCompany()));
								}
							}
							a.next();
						}
					}else{
						Compania com = new Compania();
						com.setNombre(s.getCompany());
						com.getTaxisInscritos().add(new Taxi(s.getTaxiId(),s.getCompany()));
						a.add(com);    			
					}
				}       	    
			} 
			ListaServicios.next();
		}
		a.sortByName();
		return a;
	}
	@Override //2B MARIO HURTADO
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) {
		Taxi elMasChingon = null;
		DoubleLinkedList<Taxi> t = new DoubleLinkedList<>();
		while(ListaServicios.hasNext()){
			Servicio a = ListaServicios.getCurrent();
			if(a.getCompany() != null && a.getCompany().contains(nomCompania) && a.estaEnRango2(rango)){
				Taxi tax = new Taxi(a.getTaxiId(), a.getCompany());
				Taxi elT = t.get(tax);
				if(elT != null)
					elT.servicios().add(a);
				else{
					tax.servicios().add(a);
					t.add(tax);}
			}
			ListaServicios.next();
		}
		if(!t.isEmpty()){
			elMasChingon = t.get(1);
			while(t.hasNext())
			{
				Taxi actual = t.getCurrent();
				double facturacion = darInformacionTaxiEnRango(actual.getTaxiId(), rango).getPlataGanada();
				double facturacion1 = darInformacionTaxiEnRango(elMasChingon.getTaxiId(), rango).getPlataGanada();
				if (facturacion>facturacion1){
					elMasChingon = actual;
				}
				t.next();
			}
		}
		return elMasChingon;
	}

	@Override //3B MARIO HURTADO
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona){
		ServiciosValorPagado [] arreglo = new ServiciosValorPagado[3];
		arreglo[0] =new ServiciosValorPagado();
		arreglo[1] =new ServiciosValorPagado();
		arreglo[2] =new ServiciosValorPagado();
		while(ListaServicios.hasNext()){
			Servicio a = ListaServicios.getCurrent();

			if (a.getPickArea().equals(idZona)&&a.getDropArea().equals(idZona) && a.estaEnRango(rango)){
				arreglo[0].getServiciosAsociados().add(a);
				arreglo[0].setValorAcumulado(arreglo[0].getValorAcumulado()+a.getTripTotal());

			}
			else if (a.getPickArea().equals(idZona)&& !a.getDropArea().equals(idZona) && a.estaEnRango(rango)){
				arreglo[1].getServiciosAsociados().add(a);
				arreglo[1].setValorAcumulado(arreglo[1].getValorAcumulado()+a.getTripTotal());

			}
			else if (!a.getPickArea().equals(idZona)&& a.getDropArea().equals(idZona)&&a.estaEnRango(rango)){
				arreglo[2].getServiciosAsociados().add(a);
				arreglo[2].setValorAcumulado(arreglo[2].getValorAcumulado()+a.getTripTotal());
			}
			ListaServicios.next();
		}
		return arreglo;

	}

	@Override //4B MARIO HURTADO && ANDERSON BARRAGAN
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{   DoubleLinkedList<ZonaServicios> rta = new DoubleLinkedList<ZonaServicios>();
	while(ListaServicios.hasNext())
	{
		Servicio a = ListaServicios.getCurrent();
		if ((!existeZona(a.getPickArea())||!existeZona(a.getDropArea()))&&a.estaEnRango2(rango))
		{   ZonaServicios zS= new ZonaServicios();
		FechaServicios fs = new FechaServicios();
		fs.getServiciosAsociados().add(a);
		fs.setFecha(rango.getFechaInicial());
		zS.getFechasServicios().add(fs);
		rta.add(zS);
		}
		ListaServicios.next();
	}return rta;
	}

	@Override //2C ANDERSON BARRAGAN
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n){
		DoubleLinkedList<CompaniaServicios> ccs = new DoubleLinkedList<>();
		DoubleLinkedList<CompaniaServicios> topN = new DoubleLinkedList<>();
		CompaniaServicios cs = null;
		while(ListaServicios.hasNext()){
			Servicio s = ListaServicios.getCurrent();
			if(s.getCompany() != null && s.estaEnRango2(rango)){
				cs = new CompaniaServicios();
				cs.setNomCompania(s.getCompany());
				cs.setServicios(new DoubleLinkedList<Servicio>());
				CompaniaServicios temp = ccs.get(cs);
				if(temp != null){
					temp.getServicios().add(s);
				}else{
					cs.getServicios().add(s);
					ccs.add(cs);
				}
			}
			ListaServicios.next();
		}
		if(!ccs.isEmpty()){
			CompaniaServicios mostB = ccs.get(1);
			int contador = 0;
			while(contador < n){
				CompaniaServicios cs2 = null;
				while(ccs.hasNext()){
					cs2 = ccs.next();
					if(cs2.getServicios().size() > mostB.getServicios().size())
						mostB = cs2;
				}
				topN.add(mostB);
				ccs.delete(mostB);
				contador++;
			}
		}
		return topN;
	}

	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables(){
		DoubleLinkedList<Compania> c = (DoubleLinkedList<Compania>) darCompaniasTaxisInscritos();
		DoubleLinkedList<CompaniaTaxi> rta = new DoubleLinkedList<CompaniaTaxi>();
		while(c.hasNext()){
			Compania comp = c.getCurrent();
			Taxi mejor = mejorTaxiCadaCompania(comp);
			CompaniaTaxi compT = new CompaniaTaxi();
			compT.setNomCompania(comp.getNombre());
			compT.setTaxi(mejor);
			rta.add(compT);
		}
		return rta;
	}

	@Override //4C TEAMWORK
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) {
		DoubleLinkedList<Servicio> servicios = new DoubleLinkedList<Servicio>(); 
		Stack<Servicio> sss = new Stack<Servicio>();
		RangoFechaHora r = new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
		while(ListaServicios.hasNext()){
			Servicio temp = ListaServicios.getCurrent();
			if(temp.getTaxiId().contains(taxiId) && temp.estaEnRango2(r))servicios.add(temp);
			ListaServicios.next();
		}
		if(!servicios.isEmpty()){
			servicios.sortByDate2();
			//			System.out.println(servicios.size() +"--------------------");
			while(servicios.hasNext()){
				Servicio s = servicios.getCurrent();
				sss.push(s);
				servicios.next();
			}
		}
		servicios = null;
		return sss;
	}

	@Override
	public DoubleLinkedList<Servicio> darServicios() {
		DoubleLinkedList<Servicio> d = new DoubleLinkedList<>();
		int a = 20;
		a = (ListaServicios.size() > a)?a:ListaServicios.size();
		if(!ListaServicios.isEmpty()){
			for(int i = 0; i< a;i++){
				d.add(ListaServicios.get(i+1));
			}
		}
		return d;
	}

	private boolean existeCompania(String c, DoubleLinkedList<Compania> cs){
		while (cs.hasNext()){
			Compania comp = cs.getCurrent();
			if (comp != null && comp.getNombre().equals(c)) 
				return true;
			darCompaniasTaxisInscritos().next();
		}
		return false;
	}

	private void cargaSML(Gson g, String dirJson, int tipo){
		Servicio piv = null;
		switch (tipo) {
		case 1:
			try{
				FileInputStream stream = new FileInputStream(new File(dirJson));
				JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
				reader.beginArray();
				while (reader.hasNext()) {
					Servicio srvc = g.fromJson(reader, Servicio.class);
					if(piv == null) piv = srvc;
					if(srvc.compareToDate(piv)<0)ListaServicios.addFirst(srvc);
					else ListaServicios.add(srvc);
					srvc = null;}
				reader.close();}
			catch (Exception e) {
				System.out.println("Error en la carga de los archivos peque�o");
				e.printStackTrace();}
			break;
		case 2:try{for(int i = 2; i<CANTIDAD_ARCHIVOS+2;i++){
			FileInputStream stream = new FileInputStream(new File(DIRECCION_LARGE+i+COMPLEMENT_DIR));
			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
			reader.beginArray();
			while (reader.hasNext()) {
				Servicio srvc = g.fromJson(reader, Servicio.class);
				if(piv == null) piv = srvc;
				if(srvc.compareToDate(piv)<0)ListaServicios.addFirst(srvc);
				else ListaServicios.add(srvc);
				srvc = null;}
			reader.close();}
		}catch(Exception e){System.out.println("Error en la carga del Large");e.printStackTrace();;}
		break;
		}
		System.out.println(ListaServicios.size());
	}

	private Taxi mejorTaxiCadaCompania(Compania comp)
	{
		DoubleLinkedList<Taxi> taxis = (DoubleLinkedList<Taxi>) comp.getTaxisInscritos();
		Taxi elPro = taxis.get(0);
		while (taxis.hasNext())
		{
			Taxi t = taxis.getCurrent();
			if (cuantaRelacionTaxi(t)>cuantaRelacionTaxi(elPro))
			{
				elPro = t;
			}
			taxis.next();
		}

		return elPro;
	}
	private double cuantaRelacionTaxi(Taxi t)
	{   double  plata = 0;
	double distancia = 0;
	while(ListaServicios.hasNext())
	{   Servicio a = ListaServicios.getCurrent();
	if (a.getTaxiId().equals(t.getTaxiId()))
	{
		plata += a.getTripTotal();
		distancia+= a.getTripMiles();
	}
	ListaServicios.next();
	}
	return plata/distancia;
	}
	private boolean existeZona(String zone){
		while (ListaServicios.hasNext())
		{ Servicio a = ListaServicios.getCurrent();
		if (a.getPickArea().equals(zone)||a.getDropArea().equals(zone)){
			return true;
		}
		ListaServicios.next();
		}
		return false;
	}
}
