package model.data_structures;

import model.vo.Compania;
import model.vo.Servicio;

public class DoubleLinkedList<T extends Comparable<T>> implements LinkedList<T>{

	private Node<T> root;

	private Node<T> current;

	private Node<T> head;

	private int size;

	public DoubleLinkedList() {
		root = current = head = null; size = 0;
	}

	@Override
	public boolean hasNext(){
		return (current != null)? true:false;
	}

	public boolean isEmpty(){
		return(size() == 0);
	}

	public T remove(T element){
		T toRet = (root != null)?root.get(element):null;
		if(toRet != null)delete(element);
		return toRet;
	}

	@Override
	public boolean add(T element) {
		boolean added = false;
		Node<T> newNode = new Node<T>(element);
		size++;
		if(root == null){
			root = newNode;
			current = root;
			head = root;
			added = true;
		}
		else{
			head.setNext(newNode);
			newNode.setPrev(head);
			head = newNode;
			added = true;
		}
		return added;
	}

	public boolean addFirst(T element) {
		boolean added = false;
		Node<T> newNode = new Node<T>(element);
		size++;
		if(root == null){
			root = newNode;
			current = root;
			head = root;
			added = true;
		}
		else{
			newNode.setNext(root);
			root.setPrev(newNode);
			root = newNode;
			added = true;
		}
		return added;
	}

	@Override
	public boolean delete(T element) {
		boolean deleted = false;
		Node<T> n = getNode(element);
		if(root != null && root != head && n != null){
			if(root.getItem().compareTo(element) == 0){
				root = root.next();
				root.setPrev(null);
				deleted = true;
			}else if(head.next() == null){
				head = head.previous();
				head.setNext(null);
				deleted = true;
			}
			else {
				n.setItem(null);
				n.previous().setNext(n.next());
				n.next().setPrev(n.previous());
				n.setNext(null); n.setPrev(null); n = null;
				deleted = true;
			}
		}
		return deleted;
	}

	public Node<T> getNode(T element) {
		return (root != null)? root.getNode(element):null;
	}

	@Override
	public T get(T element) {
		return (root != null)? root.get(element):null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public T get(int pos) {
		return (root != null)? root.get(pos):null;
	}

	@Override
	public T getCurrent() {
		return current.getItem();
	}

	@Override
	public T next() {
		T r = null;
		if(current != null)r = current.getItem(); current = current.next();
		return r;
	}

	@SuppressWarnings("unchecked")
	public void sortByDate(){
		//		if(root != null)root.insertionSortByDate();	
		Object[] arreglo = (new Object[size]);
		int contador = 0;
		while(hasNext()){
			T a = current.getItem();
			arreglo[contador] = a;
			contador ++;
			next();
		}
		root = head = null;
		if(arreglo.length > 0){
			sort2(arreglo);
			for (Object t : arreglo) {
				add((T)t);
			}
		}
	}

	public void sortByDate2(){
		//		if(root != null)root.insertionSortByDate();	
		Object[] arreglo = (new Object[size]);
		int contador = 0;
		while(hasNext()){
			T a = current.getItem();
			arreglo[contador] = a;
			contador ++;
			next();
		}
		root = head = null;
		if(arreglo.length > 0){
			sort4(arreglo);
			for (Object t : arreglo) {
				add((T)t);
			}
		}
	}

	public void sortByName() {
		// TODO Auto-generated method stub
		Object[] arreglo = (new Object[size]);
		int contador = 0;
		while(hasNext()){
			T a = current.getItem();
			arreglo[contador] = a;
			contador ++;
			next();
		}
		root = head = null;
		if(arreglo.length > 0){
			sort3(arreglo);
			for (Object t : arreglo) {
				add((T)t);
			}
		}
	}
	private void sort4(Object[] arreglo){
		int N = arreglo.length;
		for (int i = 1; i < N; i++) {
			for (int j = i; j > 0 && less(arreglo[j], arreglo[j-1]); j--)
				exch(arreglo, j, j-1); }
	}

	@SuppressWarnings("unchecked")
	private void exch(Object[] arreglo, int i, int j) {
		Comparable<T> t = (T)arreglo[i];
		arreglo[i] = arreglo[j];
		arreglo[j] = t; 
	}

	private void sort2(Object[] arreglo){
		int N = arreglo.length; int h = 1;
		while (h < N/3)
			h = 3*h + 1; 
		while (h >= 1) { for (int i = h; i < N; i++)
			for (int j = i; j >= h && less(arreglo[j], arreglo[j-h]); j -= h)
				exch(arreglo, j, j-h); } h = h/3;
	}

	private boolean less(Object arreglo, Object arreglo2){
		return (((Servicio) arreglo).compareToDate((Servicio)arreglo2)) < 0; }

	private void sort3(Object[] arreglo){
		int N = arreglo.length; int h = 1;
		while (h < N/3)
			h = 3*h + 1; 
		while (h >= 1) { for (int i = h; i < N; i++)
			for (int j = i; j >= h && less2(arreglo[j], arreglo[j-h]); j -= h)
				exch(arreglo, j, j-h); } h = h/3;
	}
	private boolean less2(Object arreglo, Object arreglo2){
		return (((Compania) arreglo).compareTo((Compania)arreglo2)) < 0; }
}
