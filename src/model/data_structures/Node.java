package model.data_structures;

import model.vo.Servicio;

public class Node<T extends Comparable<T>> {

	private Node<T> next;

	private Node<T> previous;

	private T MyInfo;

	public Node(T element) {
		next = 	previous = null; MyInfo = element;}

	public Node<T> next() {return next;}
	public Node<T> previous() {return previous;}

	public void setNext(Node<T> next){this.next = next;}
	public void setPrev(Node<T> prev) {previous = prev;}

	public T getItem(){return MyInfo;}
	public void setItem(T item){this.MyInfo = item;}

	public T get(T element) {return(this.MyInfo.compareTo(element) == 0)?this.getItem():
		(next != null)?next.get(element):null;}

	public T get(int pos) {return (pos == 1)? MyInfo:(next != null)?next.get(pos - 1): null;}

	public Node<T> getNode(T element) {return(this.MyInfo.compareTo(element) == 0)?this:
		(next != null)?next.getNode(element):null;}

	public void insertionSortByDate(){
		if(previous != null){
			if(((Servicio)this.getItem()).compareToDate((Servicio)previous.getItem())<0){
				T temp = previous.getItem();
				previous.setItem(this.getItem());
				this.setItem(temp);
				previous.insertionSortByDate();
			}
		}if(next != null)
			next.insertionSortByDate();
	}
}
