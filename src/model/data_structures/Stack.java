package model.data_structures;

public class Stack<T extends Comparable<T>> implements IStack<T>{

	private Node<T> top;
	private int size;

	public Stack(){top = null; size = 0;}

	public int size(){
		return size;
	}
	@Override
	public void push(T item) {
		Node<T> newNode = new Node<T>(item);
		size++;
		if(top != null)
			top = newNode;
		else{
			newNode.setNext(top);
			top = newNode;}}

	@Override
	public T pop() {
		T pop = null;
		if(top != null){
			size--;
			pop = top.getItem();
			Node<T> nuevoNodo = top.next();
			top.setNext(null);
			top = nuevoNodo;}
		return pop;}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

}
