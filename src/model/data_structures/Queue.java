package model.data_structures;

public class Queue<T extends Comparable<T>> implements IQueue<T>{

	private Node<T> foot;

	private Node<T> head;

	private int size;

	public Queue(){
		foot = head = null; size = 0;
	}

	@Override
	public void enqueue(T item) {
		Node<T> newNode = new Node<T>(item);
		size++;
		if(foot== null){
			foot = newNode;
			head = newNode;
		}else{
			newNode.setNext(foot);
			foot.setPrev(newNode);
			foot = newNode;
		}
	}

	@Override
	public T dequeue() {
		T deq = null;
		if(head != null){
			size--;
			deq = head.getItem();
			if(head == foot)
				foot = 	head = null;
			else{
				head = head.previous();
				head.setNext(null);}}
		return deq;}

	@Override
	public boolean isEmpty() {
		return (foot != null)? false: true;
		//return size ==0;
	}

	@Override
	public int size() {
		//		return (foot != null)?foot.size():0;
		return size;
	}

}
